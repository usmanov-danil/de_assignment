/**
 * Controller class
 * This class is created to handle events in working window
 */

package sample;

//Import libraries

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class Controller {

    //Objects decelerating from GUI front
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private CheckBox euler_checkbox;

    @FXML
    private CheckBox imp_euler_checkbox;

    @FXML
    private CheckBox runge_kutta_checkbox;

    @FXML
    private TextField x0_field;

    @FXML
    private TextField y0_field;

    @FXML
    private TextField X_field;

    @FXML
    private TextField n_field;

    @FXML
    private Button show_graph_button_1;

    @FXML
    private LineChart<Number, Number> graph;

    @FXML
    private LineChart<Number, Number> error_graph;

    @FXML
    private CheckBox euler_checkbox_2;

    @FXML
    private CheckBox imp_euler_checkbox_2;

    @FXML
    private CheckBox runge_kutta_checkbox_2;

    @FXML
    private TextField min_n_field;

    @FXML
    private TextField max_n_field;

    @FXML
    private Button show_graph_button_2;

    @FXML
    private LineChart<Number, Number> max_error_graph;

    @FXML
    void initialize() {
        //If show_graph_button_1 is pressed
        show_graph_button_1.setOnAction(event -> {
            //Get values from the text fields
            double x0 = Double.valueOf(x0_field.getText());
            double y0 = Double.valueOf(y0_field.getText());
            double N = Double.valueOf(n_field.getText());
            double X = Double.valueOf(X_field.getText());

            //Exception handler
            if (X < x0 || N <= 0) {
                error(); //Call error() function if some of exceptions occur
            } else {
                approximation(x0, y0, N, X); //Call approximation() function
                calculate_errors(x0, y0, N, X); //Call calculate_errors() function
            }
        });

        //If show_graph_button_2 is pressed
        show_graph_button_2.setOnAction(event -> {
            //Get values from the text fields
            double x0 = Double.valueOf(x0_field.getText());
            double y0 = Double.valueOf(y0_field.getText());
            double N = Double.valueOf(n_field.getText());
            double X = Double.valueOf(X_field.getText());
            double min_n = Double.valueOf(min_n_field.getText());
            double max_n = Double.valueOf(max_n_field.getText());

            //Exception handler
            if (X < x0 || N <= 0 || min_n > max_n) {
                error(); //Call error() function if some of exceptions occur
            } else {
                max_errors(x0, y0, N, X, min_n, max_n); //Call max_errors() function
            }

        });
    }

    /**
     * error function
     * If exception occurs then we make new throw with message "Incorrect input!"
     * Then program will close working window and open error window with warning message.
     **/
    private void error() {

        try {
            throw new Exception("Incorrect input!"); //Create new exception
        } catch (Exception e) {
            e.printStackTrace(); //Print exception
            show_graph_button_1.getScene().getWindow().hide(); //Hide working window
            FXMLLoader loader = new FXMLLoader(); //Create new loader for fxml
            loader.setLocation(getClass().getResource("/sample/fxml/error_window.fxml")); //Load error window
            try {
                loader.load();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setTitle("ERROR"); //Set title of error window
            stage.setResizable(false); //Set resizability of error window to false
            stage.setScene(new Scene(root));
            stage.showAndWait(); //Show error window
        }
    }

    /**
     * approximation function
     * This function creates graph of exact solution and numerical methods
     *
     * @param x0 is initial x
     * @param y0 is initial y
     * @param N  is the number of cells
     * @param X  is the number of finish X
     **/
    private void approximation(double x0, double y0, double N, double X) {

        Solution solutionMethod = new Solution(x0, y0, N, X); //Create new object of class Solution
        XYChart.Series solution_series = solutionMethod.solve(); //Create chart series of values of exact solution
        solution_series.setName("Solution"); //Set series name

        graph.getData().clear(); //Clear graph
        graph.getData().add(solution_series); //Add solution_series to graph

        //Free memory
        solutionMethod = null;
        solution_series = null;

        if (euler_checkbox.isSelected()) { //If euler_checkbox is selected then get series and add it to graph
            //Create new object of class EulerMethod
            EulerMethod euler_method = new EulerMethod(x0, y0, N, X);
            //Create chart series of values of Euler method
            XYChart.Series euler_series = euler_method.solve();
            euler_series.setName("Euler's method"); //Set series name
            graph.getData().add(euler_series); //Add euler_series to graph
            //Free memory
            euler_method = null;
            euler_series = null;
        }
        if (imp_euler_checkbox.isSelected()) { //If imp_euler_checkbox is selected then get series and add it to graph
            //Create chart series of values of ImprovedEulerMethod
            ImprovedEulerMethod improved_euler_method = new ImprovedEulerMethod(x0, y0, N, X);
            //Create chart series of values of improved Euler method
            XYChart.Series improved_euler_series = improved_euler_method.solve();
            improved_euler_series.setName("Imp. Euler's method"); //Set series name
            graph.getData().add(improved_euler_series); //Add improved_euler_series to graph
            //Free memory
            improved_euler_method = null;
            improved_euler_series = null;
        }
        if (runge_kutta_checkbox.isSelected()) { //If runge_kutta_checkbox is selected then get series and add it to graph
            //Create new object of class RungeKuttaMethod
            RungeKuttaMethod runge_kutta_method = new RungeKuttaMethod(x0, y0, N, X);
            //Create chart series of values of Runge_Kutta_method
            XYChart.Series runge_kutta_series = runge_kutta_method.solve();
            runge_kutta_series.setName("Runge-Kutta method"); //Set series name
            graph.getData().add(runge_kutta_series); //Add runge_kutta_series to graph
            //Free memory
            runge_kutta_method = null;
            runge_kutta_series = null;
        }
        System.gc();
    }

    /**
     * calculate_errors function
     * This function creates graph of errors between exact solution and numerical methods
     *
     * @param x0 is initial x
     * @param y0 is initial y
     * @param N  is the number of cells
     * @param X  is the number of finish X
     */
    private void calculate_errors(double x0, double y0, double N, double X) {

        error_graph.getData().clear(); //Clear graph

        if (euler_checkbox.isSelected()) { //If euler_checkbox is selected then get series and add it to graph
            //Create new object of class EulerMethod
            EulerMethod euler_method = new EulerMethod(x0, y0, N, X);
            //Create chart series of values of errors of Euler method
            XYChart.Series euler_series = euler_method.calc_errors();
            euler_series.setName("Euler method`s errors"); //Set series name
            error_graph.getData().add(euler_series); //Add euler_series to graph
            //Free memory
            euler_method = null;
            euler_series = null;
        }
        if (imp_euler_checkbox.isSelected()) { //If imp_euler_checkbox is selected then get series and add it to graph
            //Create new object of class ImprovedEulerMethod
            ImprovedEulerMethod improved_euler_method = new ImprovedEulerMethod(x0, y0, N, X);
            //Create chart series of values of errors of Improved Euler method
            XYChart.Series improved_euler_series = improved_euler_method.calc_errors();
            improved_euler_series.setName("Improved Euler method`s errors"); //Set series name
            error_graph.getData().add(improved_euler_series); //Add improved_euler_series to graph
            //Free memory
            improved_euler_method = null;
            improved_euler_series = null;
        }
        if (runge_kutta_checkbox.isSelected()) { //If runge_kutta_checkbox is selected then get series and add it to graph
            //Create new object of class RungeKuttaMethod
            RungeKuttaMethod runge_kutta_method = new RungeKuttaMethod(x0, y0, N, X);
            //Create chart series of values of errors of Runge-Kutta method
            XYChart.Series runge_kutta_series = runge_kutta_method.calc_errors(); //Set series name
            runge_kutta_series.setName("Runge-Kutta method`s errors");
            error_graph.getData().add(runge_kutta_series); //Add runge_kutta_series to graph
            //Free memory
            runge_kutta_method = null;
            runge_kutta_series = null;
        }
        System.gc();
    }

    /**
     * max_errors function
     * This function creates graph of max errors
     *
     * @param x0    is initial x
     * @param y0    is initial y
     * @param N     is the number of cells
     * @param X     is the number of finish X
     * @param min_n is the starting N
     * @param max_n is the finishing N
     */
    private void max_errors(double x0, double y0, double N, double X, double min_n, double max_n) {

        //Create new objects of classes: EulerMethod, ImprovedEulerMethod, RungeKuttaMethod
        EulerMethod euler_method = new EulerMethod(x0, y0, N, X);
        ImprovedEulerMethod improved_euler_method = new ImprovedEulerMethod(x0, y0, N, X);
        RungeKuttaMethod runge_kutta_method = new RungeKuttaMethod(x0, y0, N, X);

        //Create chart series of values of Euler method, improved Euler method, Runge-Kutta method
        XYChart.Series max_euler_series = new XYChart.Series();
        XYChart.Series max_improved_euler_series = new XYChart.Series();
        XYChart.Series max_runge_kutta_series = new XYChart.Series();

        //Set names of series
        max_euler_series.setName("Euler method`s max errors");
        max_improved_euler_series.setName("Improved euler method`s max errors");
        max_runge_kutta_series.setName("Runge-Kutta method`s max errors");

        //Find max error for each method with different n
        for (int i = (int) min_n; i < (int) max_n + 1; i++) {
            max_euler_series.getData().add(new XYChart.Data(i, euler_method.find_max_error(i)));
            max_improved_euler_series.getData().add(new XYChart.Data(i, improved_euler_method.find_max_error(i)));
            max_runge_kutta_series.getData().add(new XYChart.Data(i, runge_kutta_method.find_max_error(i)));
        }

        max_error_graph.getData().clear(); //Clear graph

        if (euler_checkbox_2.isSelected()) { //If euler_checkbox_2 is selected then add series to graph
            max_error_graph.getData().add(max_euler_series); //Add max_euler_series to graph
        }
        if (imp_euler_checkbox_2.isSelected()) { //If imp_euler_checkbox_2 is selected then add series to graph
            max_error_graph.getData().add(max_improved_euler_series); //Add max_improved_euler_series to graph
        }
        if (runge_kutta_checkbox_2.isSelected()) { //If runge_kutta_checkbox_2 is selected then add series to graph
            max_error_graph.getData().add(max_runge_kutta_series); //Add max_runge_kutta_series to graph
        }
        //Free memory
        euler_method = null;
        improved_euler_method = null;
        runge_kutta_method = null;
        max_euler_series = null;
        max_improved_euler_series = null;
        max_runge_kutta_series = null;
        System.gc();
    }
}