/**
 * ErrorController class
 * This class is created to handle events in error window
 */

package sample;

//Import libraries

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class ErrorController {

    //Objects decelerating from GUI front
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button return_button;

    @FXML
    void initialize() {

        //If return_button is pressed
        return_button.setOnAction(ActionEvent -> {
            return_button.getScene().getWindow().hide(); //Hide error window
            FXMLLoader loader = new FXMLLoader(); //Create new loader for fxml
            loader.setLocation(getClass().getResource("/sample/fxml/sample.fxml")); //Load working window
            try {
                loader.load();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setTitle("DE: Assignment"); //Set title of working window
            stage.setResizable(false); //Set resizability of working window to false
            stage.setScene(new Scene(root));
            stage.show(); //Show working window
        });
    }
}
