/**
 * Solution class
 * This class is created to solve equation with exact solution
 */

package sample;

//Import library

import javafx.scene.chart.XYChart;

class Solution {

    private double x_0; //initial x
    private double y_0; //initial y
    private double X; //The number of finish x
    private double N; //The number of cells
    private double h; //The step
    private double[] x_i; //Array of x
    private double[] y_i; //Array of y

    //Constructor
    Solution(double x_0, double y_0, double N, double X) {
        y_i = new double[(int) N + 1]; //Create array of size int(N) + 1
        x_i = new double[(int) N + 1]; //Create array of size int(N) + 1

        //Set values
        this.x_0 = x_0;
        this.y_0 = y_0;
        this.X = X;
        this.N = N;
        h = (X - x_0) / N; //Calculate h
    }

    //constant() function calculates C for given initial values
    double constant(double x, double y) {
        return y / (Math.pow(x, 2) * Math.exp(-(3 / x)));
    }

    //getDataY() returns array of y
    double[] getDataY() {
        return y_i;
    }

    //solve() function solves equation with exact solution
    XYChart.Series solve() {

        XYChart.Series solution_series = new XYChart.Series(); //Create new series
        for (int i = 0; i < (int) N + 1; i++) {
            if (i == 0) { //If base step then put initial values
                x_i[i] = x_0;
                y_i[i] = y_0;
            } else { //Else calculate x and y with exact solution
                x_i[i] = x_i[i - 1] + h;
                y_i[i] = constant(x_0, y_0) * Math.pow(x_i[i], 2) * Math.exp(-(3 / x_i[i]));
            }

            solution_series.getData().add(new XYChart.Data(x_i[i], y_i[i])); //Add finite arrays to series
        }


        return solution_series;
    }

}
