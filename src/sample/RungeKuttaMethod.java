/**
 * RungeKuttaMethod class
 * This class is created to solve equation with Runge-Kutta method
 */

package sample;

//Import library

import javafx.scene.chart.XYChart;

class RungeKuttaMethod implements Method {

    private double x_0; //initial x
    private double y_0; //initial y
    private double X; //The number of finish x
    private double N; //The number of cells
    private double h; //The step
    private double[] x_i; //Array of x
    private double[] y_i; //Array of y

    //Constructor
    RungeKuttaMethod(double x_0, double y_0, double N, double X) {
        x_i = new double[(int) N + 1]; //Create array of size int(N) + 1
        y_i = new double[(int) N + 1]; //Create array of size int(N) + 1

        //Set values
        this.x_0 = x_0;
        this.y_0 = y_0;
        this.X = X;
        this.N = N;
        h = (X - x_0) / N; //Calculate h
    }

    //constant() function calculates C for given initial values
    private double constant(double x, double y) {
        return y / (Math.pow(x, 2) * Math.exp(-(3 / x)));
    }

    //f1() function calculates y for given x
    private double f1(double x, double y) {
        return (3 * y + 2 * x * y) / Math.pow(x, 2);
    }

    //f2() function calculates k2
    private double f2(double x, double y, double h) {
        double f1 = f1(x, y);
        return f1(x + (h / 2), y + (h * f1) / 2);

    }

    //f3() function calculates k3
    private double f3(double x, double y, double h) {
        double f2 = f2(x, y, h);
        return f1(x + (h / 2), y + (h * f2) / 2);
    }

    //f4() function calculates k4
    private double f4(double x, double y, double h) {
        double f3 = f3(x, y, h);
        return f1(x + h, y + h * f3);
    }

    //calc_y_i() function calculate y fo given x and coefficients
    private double calc_y_i(int i) {
        double f1 = f1(x_i[i - 1], y_i[i - 1]);
        double f2 = f2(x_i[i - 1], y_i[i - 1], h);
        double f3 = f3(x_i[i - 1], y_i[i - 1], h);
        double f4 = f4(x_i[i - 1], y_i[i - 1], h);

        return (y_i[i - 1] + (h / 6) * (f1 + (f2 * 2) + (f3 * 2) + f4));
    }

    //reinitialize() function reinitialize N, h and arrays of x, y
    private void reinitialize(double number_of_steps) {
        N = number_of_steps;
        h = (X - x_0) / number_of_steps;
        y_i = new double[(int) number_of_steps + 1];
        x_i = new double[(int) number_of_steps + 1];
    }

    //solve() function solves equation with Runge-Kutta method
    public XYChart.Series solve() {

        XYChart.Series runge_kutta_series = new XYChart.Series(); //Create new series
        for (int i = 0; i < (int) N + 1; i++) { //Calculate x and y
            if (i == 0) { //If base step then put initial values
                x_i[i] = x_0;
                y_i[i] = y_0;
            } else { //Else calculate x and y with Runge-Kutta method
                x_i[i] = x_i[i - 1] + h;
                y_i[i] = calc_y_i(i);
            }
            runge_kutta_series.getData().add(new XYChart.Data(x_i[i], y_i[i])); //Add finite arrays to series
        }

        return runge_kutta_series;
    }

    //calc_errors() function calculates errors between exact solution and Runge-Kutta method
    public XYChart.Series calc_errors() {
        XYChart.Series runge_kutta_series = new XYChart.Series(); //Create new series
        solve(); //Solve equation with Runge-Kutta method
        Solution solution = new Solution(x_0, y_0, N, X); //Create new object of class Solution
        solution.solve(); //Solve equation with exact solution
        double[] y_sol = solution.getDataY(); //Get y from exact solution

        for (int i = 0; i < (int) N + 1; i++) {
            runge_kutta_series.getData().add(new XYChart.Data(x_i[i], y_sol[i] - y_i[i])); //Error is difference
        }

        return runge_kutta_series;
    }

    //find_max_error() function calculates max errors between Runge-Kutta method and exact solution
    public double find_max_error(int number_of_steps) {
        double max = 0; //max variable for handle max value

        reinitialize((double) number_of_steps); //Reinitialize arrays
        Solution solution = new Solution(x_0, y_0, N, X); //Create new object of class Solution
        solution.solve(); //Solve exact solution
        double[] solY = solution.getDataY(); //Get y from exact solution
        solve(); //Solve equation with Runge-Kutta method

        for (int i = 0; i < N + 1; i++) { //Find max error
            if (Math.abs(solY[i] - y_i[i]) > max) {
                max = Math.abs(y_i[i] - solY[i]);
            }
        }

        return max;
    }
}
