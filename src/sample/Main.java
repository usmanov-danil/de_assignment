/**
 * Differential equation assignment
 * <p>
 * Contact:
 * Danil Usmanov BS18-01 group
 * email: d.usmanov@innopolis.university
 * telegram: @usmanov_d
 **/

package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("fxml/sample.fxml")); //Load window file
        primaryStage.setTitle("DE: Assignment"); //Set title of working window
        primaryStage.setScene(new Scene(root, 800, 600)); //Set size of window
        primaryStage.setResizable(false); //Set resizability of working window to false
        primaryStage.show(); //Show this window
    }


    public static void main(String[] args) {
        launch(args);
    }
}
