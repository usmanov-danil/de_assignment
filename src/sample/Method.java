/**
 * Interface of methods
 */

package sample;

//Import library

import javafx.scene.chart.XYChart;

public interface Method {
    private void reinitialize(double num_of_steps) { //reinitialize() is used to reinitialize arrays of x and y
    }

    private double constant(double x, double y) { //constant() is used to calculate C for initial values
        return y / (Math.pow(x, 2) * Math.exp(-(3 / x)));
    }

    double find_max_error(int num_of_steps); //find_max_error is used to find max error

    XYChart.Series calc_errors(); //calc_errors() is used to calculate errors

    XYChart.Series solve(); //solve() is used to solve equation
}
