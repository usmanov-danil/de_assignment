/**
 * ImprovedEulerMethod class
 * This class is created to solve equation with improved Euler method
 */

package sample;

//Include library

import javafx.scene.chart.XYChart;


class ImprovedEulerMethod implements Method {

    private double x_0; //initial x
    private double y_0; //initial y
    private double X; //The number of finish x
    private double N; //The number of cells
    private double h; //The step
    private double[] x_i; //Array of x
    private double[] y_i; //Array of y

    //Constructor
    ImprovedEulerMethod(double x_0, double y_0, double N, double X) {
        x_i = new double[(int) N + 1]; //Create array of size int(N) + 1
        y_i = new double[(int) N + 1]; //Create array of size int(N) + 1

        //Set values
        this.x_0 = x_0;
        this.y_0 = y_0;
        this.X = X;
        this.N = N;
        h = (X - x_0) / N; //Calculate h
    }

    //constant() function calculates C for given initial values
    private double constant(double x, double y) {
        return y / (Math.pow(x, 2) * Math.exp(-(3 / x)));
    }

    //f() function calculates y for given x
    private double f(double x, double y) {
        return (3 * y + 2 * x * y) / Math.pow(x, 2);
    }

    //reinitialize() function reinitialize N, h and arrays of x, y
    private void reinitialize(double numberOfSteps) {
        N = numberOfSteps;
        h = (X - x_0) / numberOfSteps;
        y_i = new double[(int) numberOfSteps + 1];
        x_i = new double[(int) numberOfSteps + 1];
    }

    //solve() function solves equation with improved Euler method
    public XYChart.Series solve() {

        XYChart.Series improved_euler_series = new XYChart.Series(); //Create new series
        for (int i = 0; i < (int) N + 1; i++) { //Calculate x and y
            if (i == 0) { //If base step then put initial values
                x_i[i] = x_0;
                y_i[i] = y_0;
            } else { //Else calculate x and y with improved Euler method method
                x_i[i] = x_i[i - 1] + h;
                y_i[i] = y_i[i - 1] + (h * f(x_i[i - 1] + (h / 2), y_i[i - 1] + ((h / 2) * f(x_i[i - 1], y_i[i - 1]))));
            }
            improved_euler_series.getData().add(new XYChart.Data(x_i[i], y_i[i])); //Add finite arrays to series
        }

        return improved_euler_series;
    }

    //calc_errors() function calculates errors between exact solution and improved Euler method
    public XYChart.Series calc_errors() {
        XYChart.Series improved_euler_series = new XYChart.Series(); //Create new series
        solve(); //Solve equation with improved Euler method
        Solution solution = new Solution(x_0, y_0, N, X); //Create new object of class Solution
        solution.solve(); //Solve equation with exact solution
        double[] y_sol = solution.getDataY(); //Get y from exact solution

        for (int i = 0; i < (int) N + 1; i++) { //Calculate errors
            improved_euler_series.getData().add(new XYChart.Data(x_i[i], y_sol[i] - y_i[i])); //Error is difference
        }

        return improved_euler_series;
    }

    //find_max_error() function calculates max errors between improved Euler method and exact solution
    public double find_max_error(int number_of_steps) {
        double max = 0; //max variable for handle max value

        reinitialize((double) number_of_steps); //Reinitialize arrays
        Solution solution = new Solution(x_0, y_0, N, X); //Create new object of class Solution
        solution.solve(); //Solve exact solution
        double[] y_sol = solution.getDataY(); //Get y from exact solution
        solve(); //Solve equation with improved Euler method

        for (int i = 0; i < (int) N + 1; i++) { //Find max error
            if (Math.abs(y_i[i] - y_sol[i]) > max) {
                max = Math.abs(y_i[i] - y_sol[i]);
            }
        }

        return max;
    }
}
